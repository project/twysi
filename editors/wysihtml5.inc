<?php

/**
 * @file
 * Editor integration functions for bootstrap-wysihtml5.
 */

/**
 * Implements hook_editor().
 */
function twysi_wysihtml5_editor() {
  $path = drupal_get_path('module', 'twysi');

  // Try to load the library and check if that worked.
  if (!(function_exists('libraries_detect') && libraries_detect('bootstrap') && ($library = libraries_load('bootstrap')) && !empty($library['loaded']))) {
    drupal_add_js($path . '/bootstrap/js/bootstrap.min.js');
    drupal_add_css($path . '/bootstrap/css/bootstrap.min.css');
  }

  drupal_add_css($path . '/editors/css/wysihtml5.css');

  $editor['wysihtml5'] = array(
    'title' => 'wysihtml5',
    'vendor url' => 'http://jhollingworth.github.com/bootstrap-wysihtml5/',
    'download url' => 'https://github.com/jhollingworth/bootstrap-wysihtml5/',
    'libraries' => array(
      '' => array(
        'title' => 'Source',
        'files' => array('lib/js/wysihtml5-0.3.0.min.js', 'src/bootstrap-wysihtml5.js'),
      ),
    ),
    'version callback' => 'twysi_wysihtml5_version',
    'css path' => wysiwyg_get_path('wysihtml5'),
    'versions' => array(
      '0.0.2' => array(
        'js files' => array('wysihtml5.js'),
        'css files' => array('src/bootstrap-wysihtml5.css'),
      ),
    ),
  );
  
  return $editor;
}

/**
 * Implements hook_version().
 */
function twysi_wysihtml5_version($editor) {
  $script = $editor['library path'] . '/VERSION';
  if (!file_exists($script)) {
    return;
  }

  return file_get_contents($script);
}
